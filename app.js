var express = require("express");
var MongoClient = require("mongodb").MongoClient;
var ObjectID = require("mongodb").ObjectId;
var bodyParser = require("body-parser");
var assert = require("assert");
var logger = require("morgan");
var path = require("path");
var Pusher = require("pusher");
var cors = require("cors");
var port = 8080;
var mongoUri = "mongodb://localhost:27017/";

var app = express();

app.use(logger("dev"));
app.set("view engine", "html");
app.set("views", path.join(__dirname, "views"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

MongoClient.connect(mongoUri, { useNewUrlParser: true }, function(err, client) {
  if (err) {
    res
      .status(500)
      .send({ message: "FAILED", message: "Error in mongo connection" });
  } else {
    console.log("Successfully connected to mondodb");

    var db = client.db("stocks");

    app.get("/", async function(req, res) {
      res.render("index.html");
    });

    app.post("/addbook", function(req, res) {
      let requestBody = req.body;
      if (!requestBody.author || !requestBody.author) {
        res
          .status(500)
          .send({ status: "FAILED", message: "Required params doesn't exist" });
      } else {
        db.collection("books").insertOne(
          {
            name: requestBody.book,
            author: requestBody.author,
            lended: false,
            lended_on: "",
            returned: false,
            returned_on: "",
            date: new Date()
          },
          (err, result) => {
            if (err)
              res
                .status(500)
                .json({ status: "FAILED", message: "Internal Server Error" });
            else {
              res.json({ status: "OK", message: "Book added" });
            }
          }
        );
      }
    });

    app.get("/stock", async function(req, res) {
      let booksInStocks = await db
        .collection("books")
        .find({ lended: false })
        .toArray();

      res.json({ status: "OK", data: booksInStocks });
    });

    app.get("/lended", async function(req, res) {
      let lendedBooks = await db
        .collection("books")
        .find({ lended: true })
        .toArray();

      res.json({ status: "OK", data: lendedBooks });
    });

    app.post("/tolend", function(req, res) {
      let requestBody = req.body;
      if (!requestBody.bookId) {
        res
          .status(500)
          .send({ status: "FAILED", message: "Required params doesn't exist" });
      } else {
        db.collection("books").updateOne(
          { _id: ObjectID(requestBody.bookId) },
          {
            $set: {
              lended: true,
              lended_on: new Date()
            }
          },
          (err, result) => {
            if (err)
              res
                .status(500)
                .json({ status: "FAILED", message: "Internal Server Error" });
            else {
              res.json({ status: "OK", message: "Book supplied" });
            }
          }
        );
      }
    });

    app.get("/search", async function(req, res) {
      if (
        Object.entries(req.query).length !== 0 &&
        req.query.constructor === Object
      ) {
        let searchKeys = Object.keys(req.query);
        let searchKey = searchKeys[0];

        let searchParams = {};
        searchParams[searchKey] = new RegExp(req.query[searchKey], "i");
        let books = await db
          .collection("books")
          .find(searchParams)
          .toArray();

        res.json({ status: "OK", data: books });
      } else {
        res.status(500).json({ status: "FAILED", message: "Invalid search" });
      }
    });

    app.post("/return", function(req, res) {
      let bookId = req.body.bookId;

      db.collection("books").updateOne(
        { _id: ObjectID(bookId) },
        {
          $set: {
            lended: false,
            lended_on: "",
            returned: true,
            returned_on: new Date()
          }
        },
        (err, result) => {
          if (err)
            res
              .status(500)
              .json({ status: "FAILED", message: "Internal Server Error" });
          else {
            var pusher = new Pusher({
              appId: "706242",
              key: "6173d2f970f77a518d66",
              secret: "1cdfa0ff8870e7ccbe62",
              cluster: "ap2"
            });

            pusher.trigger("my-channel", "my-event", "A book returned");

            res.json({ status: "OK", message: "Book returned" });
          }
        }
      );
    });

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      var err = new Error("Not Found");
      err.status = 404;
      next(err);
    });

    app.listen(port, function() {
      console.log("Server listening on port " + port);
    });
  }
});
